package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.DBConstants.*;

public class DBManager {
	private static final String APP_PROPERTIES = "app.properties";
	private static final String CONNECTION_URL = "connection.url";
	private static final Logger logger = Logger.getLogger(Exception.class.getName());;

	private static DBManager instance;
	private static String url;

	private DBManager() {
	}
	public static synchronized DBManager getInstance() {
		if(instance == null){
			url = getURL();
			instance = new DBManager();
		}
		return instance;
	}

	public static String getURL() {
		if (url == null) {
			var prop = new Properties();
			try (InputStream inputStream = new FileInputStream(APP_PROPERTIES)) {
				prop.load(inputStream);
			} catch (IOException e) {
				logger.log(Level.SEVERE,e.getMessage());
			}
			url = prop.getProperty(CONNECTION_URL);
		}
		return url;
	}

	public boolean insertUser(User user) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(INSERT_USER,Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1,user.getLogin());
			statement.execute();
			var resultSet = statement.getGeneratedKeys();
			if (resultSet.next()){
				user.setId(resultSet.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SELECT_USERS)) {
			statement.execute();
			var resultSet = statement.getResultSet();
			while (resultSet.next()){
				var newUser = new User();
				newUser.setId(resultSet.getInt(F_USER_ID));
				newUser.setLogin(resultSet.getString(F_USER_LOGIN));
				users.add(newUser);
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
		return users;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);) {
			statement.setString(1,team.getName());
			statement.execute();
			var resultSet = statement.getGeneratedKeys();
			if (resultSet.next()){
				team.setId(resultSet.getInt(1));
			}
			return true;
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SELECT_TEAMS)) {
			statement.execute();
			var resultSet = statement.getResultSet();
			while (resultSet.next()){
				var newTeam = new Team();
				newTeam.setId(resultSet.getInt(F_TEAM_ID));
				newTeam.setName(resultSet.getString(F_TEAM_NAME));
				teams.add(newTeam);
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
		return teams;
	}

	public User getUser(String login) throws DBException {
		var getUser = new User();
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SELECT_USER_BY_LOGIN)) {
			statement.setString(1,login);
			var resultSet = statement.executeQuery();
			resultSet.next();
			getUser.setId(resultSet.getInt(F_USER_ID));
			getUser.setLogin(resultSet.getString(F_USER_LOGIN));
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
		return getUser;
	}

	public Team getTeam(String name) throws DBException {
		var getTeam = new Team();
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SELECT_TEAM_BY_NAME);) {
			statement.setString(1,name);
			var resultSet = statement.executeQuery();
			resultSet.next();
			getTeam.setId(resultSet.getInt(F_TEAM_ID));
			getTeam.setName(resultSet.getString(F_TEAM_NAME));
		} catch (SQLException e) {
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		}
		return getTeam;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null || teams == null || teams.length == 0){
			return false;
		}
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(url);
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(INSERT_INTO_USERS_TEAMS);
			statement.setInt(1,user.getId());
			for (Team team:teams){
				statement.setInt(2,team.getId());
				statement.execute();
			}
			connection.commit();
			return true;
		} catch (SQLException e) {
			try {
				if (connection != null)
					connection.rollback();
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(),e);
			}
			logger.log(Level.SEVERE,e.getMessage());
			throw new DBException(e.getMessage(),e);
		} finally {
			try {
				assert connection != null;
				connection.close();
				assert statement != null;
				statement.close();
			} catch (SQLException e) {
				logger.log(Level.SEVERE,e.getMessage());
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(SELECT_USER_TEAMS)) {
			statement.setInt(1,user.getId());
			statement.executeQuery();
			var resultSet = statement.getResultSet();
			while (resultSet.next()){
				var newTeam = new Team();
				newTeam.setId(resultSet.getInt(F_TEAM_ID));
				newTeam.setName(resultSet.getString(F_TEAM_NAME));
				teams.add(newTeam);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(),e);
		}
		return teams;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(DELETE_USERS)){
			for (User user:users){
				statement.setInt(1,user.getId());
				statement.execute();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(DELETE_TEAM);) {
			statement.setString(1,team.getName());
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(),e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (var connection = DriverManager.getConnection(url);
			 var statement = connection.prepareStatement(UPDATE_TEAM);){
			statement.setString(1,team.getName());
			statement.setInt(2,team.getId());
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(),e);
		}
	}

}
