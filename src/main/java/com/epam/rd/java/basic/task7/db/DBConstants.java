package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    private DBConstants(){}
    public static final String F_USER_ID = "id";
    public static final String F_USER_LOGIN = "login";
    public static final String F_TEAM_ID = "id";
    public static final String F_TEAM_NAME = "name";

    public static final String DELETE_USERS = "delete from users where id = ?";
    public static final String INSERT_USER = "insert into users(login) values (?)";
    public static final String SELECT_USERS = "select * from users";
    public static final String INSERT_TEAM = "insert into teams(name) values (?)";
    public static final String SELECT_TEAMS = "select * from teams";
    public static final String SELECT_USER_BY_LOGIN = "select * from users where login = ?";
    public static final String SELECT_TEAM_BY_NAME = "select * from teams where name = ?";
    public static final String INSERT_INTO_USERS_TEAMS = "insert into users_teams (user_id,team_id) values(?,?)";
    public static final String SELECT_USER_TEAMS = "select * from teams where id in (select team_id from users_teams where user_id = ?)";
    public static final String DELETE_TEAM = "delete from teams where name = ?";
    public static final String UPDATE_TEAM = "update teams set name = ? where id = ?";
}
